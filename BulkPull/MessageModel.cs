﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BulkPull
{
    public class MessageModel
    {
        public const string SUCCESS = "Success";
        public const string ADMINISTRTOR_ERROR = "Something goes wrong. Please contact to administrator";
        public const string NO_RECORD_FOUND = "No record found";
        public const string INTERFACE_CONFIG_ERROR = "Please configure the destination for the interface";

    }

}
