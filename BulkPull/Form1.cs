using BulkPull.Model;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace BulkPull
{
    public partial class Form1 : Form
    {
        private string _dbConnection = string.Empty; // ConfigurationManager.AppSettings["DBConnection"]; 
                                                     //private string _dbConnection = ConfigurationManager.AppSettings["DBConnection"];

        private string SqlType = "";

        public Form1()
        {
            InitializeComponent();
        }

        public Form1(string _connecttion)
        {
            _dbConnection = _connecttion;
            InitializeComponent();
        }



        public static string _filePath { get; set; }
        public static string _targetSQLDb { get; set; }

        private void UploadButton_Click(object sender, EventArgs e)
        {
            string _Output = string.Empty;
            var _OutputResponse = new OutputResponseModel();
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();
            try
            {
                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
                var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageCompanyTable"];
                var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureCompanyQueue"];
                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];

                string _outResponse = string.Empty;

                // getting data from database 

                int pageSize = 100;

                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();

                int i = 0;

                connetionString = _dbConnection;
                StringBuilder sqlQuery = new StringBuilder();
                connection = new SqlConnection(connetionString);

                try
                {
                    sqlQuery = DropCompanyProcedure();

                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("procedure has dropped");
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Dropped" + ex.Message);
                    connection.Close();
                }

                try
                {
                    sqlQuery = DropFetchcompanyProcedure();

                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("fetch procedure has dropped");
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Dropped" + ex.Message);
                    connection.Close();
                }

                sqlQuery.Clear();

                try
                {
                    sqlQuery = companyProfileprocedureString();
                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("Created the procedure");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Created" + ex.Message);
                    connection.Close();
                }


                sqlQuery.Clear();


                try
                {
                    sqlQuery = FetchCompanyprocedureString();
                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("Fetch Created the procedure");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Created" + ex.Message);
                    connection.Close();
                }


                try
                {
                    for (int pageNumber = 1; ; pageNumber++)
                    {
                        connection.Open();
                        ds.Clear();
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@pagenumber", pageNumber);
                        command.Parameters.AddWithValue("@pagesize", pageSize);
                        command.CommandText = "[fetchbulkCompanyProfile]";
                        adapter = new SqlDataAdapter(command);
                        adapter.Fill(ds);
                        connection.Close();
                        MessageBox.Show("Executed the procedure to fetch data");

                        rtStatus.Text = "Company Profile Trasfering Started";
                        rtStatus.ForeColor = Color.Green;
                        rtStatus.BackColor = Color.White;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            exportCompanyData(ds);
                            continue;
                        }
                        else
                        {
                            rtStatus.Text = "Company Profile Trasfering End ";
                            rtStatus.ForeColor = Color.Green;
                            rtStatus.BackColor = Color.White;
                            break;
                        }

                    }

                    MessageBox.Show(" Data Exported Successfully !! ");

                }
                catch (Exception ex)
                {
                    connection.Close();
                    MessageBox.Show("Executed" + ex.Message);
                }

            }
            catch (Exception ex)
            {
                _Output = ex.Message.ToString();
            }
        }



        private async Task QueueExists(string QueueName, string ServiceBusConnectionString)
        {
            using (var _QueueHandler = new QueueHandler(ServiceBusConnectionString))
            {
                await _QueueHandler.QueueExists(QueueName);
            }
        }

        private void GetFuncationalityValue()
        {

            comboDBList.Items.Add("--Select--");
            comboDBList.Items.Add("Company");
            comboDBList.Items.Add("Reservation");
            comboDBList.Items.Add("Guest");
            comboDBList.Items.Add("HotelPosition");
            comboDBList.SelectedIndex = 0;
        }

        private void GetSQLTypeValue()
        {
            comboBoxSQLType.Items.Add("--Select--");
            comboBoxSQLType.Items.Add("New");
            comboBoxSQLType.Items.Add("Old");
            comboBoxSQLType.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            string DB = _dbConnection;
            GetFuncationalityValue();
            GetSQLTypeValue();
        }

        private void comboDBList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboDBList.SelectedIndex == 0)
            {
                groupBox1.Enabled = true;
                groupBox2.Enabled = true;
                groupBox3.Enabled = true;
                groupBox4.Enabled = true;
            }
            else if (comboDBList.SelectedIndex == 1)
            {
                groupBox1.Enabled = true;
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;
                groupBox4.Enabled = false;

            }
            else if (comboDBList.SelectedIndex == 2)
            {
                groupBox1.Enabled = false;
                groupBox2.Enabled = true;
                groupBox3.Enabled = false;
                groupBox4.Enabled = false;
            }
            else if (comboDBList.SelectedIndex == 3)
            {
                groupBox1.Enabled = false;
                groupBox2.Enabled = false;
                groupBox3.Enabled = true;
                groupBox4.Enabled = false;
            }
            else if (comboDBList.SelectedIndex == 4)
            {
                groupBox1.Enabled = false;
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;
                groupBox4.Enabled = true;
            }
            //else
            //{
            //    groupBox1.Enabled = false;
            //    groupBox2.Enabled = false;
            //    groupBox3.Enabled = true;
            //    groupBox4.Enabled = false;
            //}


            using (StreamWriter w = File.AppendText("log.txt"))
            {
                LogControl.Log(comboDBList.SelectedText.ToString(), w);
            }

            using (StreamReader r = File.OpenText("log.txt"))
            {
                StringBuilder sb = new StringBuilder();
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    sb.AppendLine(line.ToString());
                }

                //rtLog.Text = sb.ToString();
            }




        }
        private void btnHotelPosition_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");


            btn_ReservationExport.Enabled = false;
            btn_ReservationExport.ForeColor = Color.Green;


            string _Output = string.Empty;
            var _OutputResponse = new OutputResponseModel();
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();

            try
            {
                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];

                //needed
                //var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageReservationTable"];
                //var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureReservationQueue"];

                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];

                string _outResponse = string.Empty;

                int pageSize = 100;

                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();

                connetionString = _dbConnection;
                StringBuilder sqlQuery = new StringBuilder();
                connection = new SqlConnection(connetionString);

                //procedures needed
                try
                {
                    sqlQuery = DropHotelPositionProcedure();

                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("procedure has dropped");
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Dropped" + ex.Message);
                    connection.Close();
                }

                try
                {
                    sqlQuery = DropFetchHotelPositionProcedure();

                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("fetch procedure has dropped");
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Dropped" + ex.Message);
                    connection.Close();
                }

                sqlQuery.Clear();

                try
                {
                    sqlQuery = HotelPositionprocedureString();
                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("Created the procedure");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Created" + ex.Message);
                    connection.Close();
                }


                try
                {
                    sqlQuery = HotelPositionFetchprocedureString();
                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("Fetch Created the procedure");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Created" + ex.Message);
                    connection.Close();
                }

                try
                {
                    for (int pageNumber = 1; ; pageNumber++)
                    {
                        connection.Open();
                        ds.Clear();
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("@pagenumber", pageNumber);
                        command.Parameters.AddWithValue("@pagesize", pageSize);
                        //procedure name need to change.
                        command.CommandText = "[fetchbulkrReservation]";
                        adapter = new SqlDataAdapter(command);
                        adapter.Fill(ds);
                        connection.Close();
                        MessageBox.Show("Executed the procedure to fetch data");
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            exportHotelPositionData(ds);
                            continue;
                        }
                        else
                        {
                            break;
                        }

                    }

                    MessageBox.Show(" Data Exported Successfully !! ");

                }
                catch (Exception ex)
                {
                    connection.Close();
                    MessageBox.Show("Executed" + ex.Message);
                }

            }
            catch (Exception ex )
            {

                 _Output = ex.Message.ToString();
            }

        }

        private void exportHotelPositionData(DataSet hotelPosition)
        {
            var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
            var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
            //var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageReservationTable"];
            //var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureReservationQueue"];
            var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];


            string _outResponse = string.Empty;

            var _OutputResponse = new OutputResponseModel();

            // data has fetched from db

            //need to change the model.
            List<PMSHotelPositionRequest> lstPmsReservation = hotelPosition.Tables[0].AsEnumerable().Select(
                        dataRow => new PMSHotelPositionRequest
                        {
                            //need to change
                            SerialNo = dataRow.Field<int>("ROWNUB"),
                            PropertyCode = dataRow.Field<long>("PMSCODE"),
                            Rundate = dataRow.Field<string>("RESNUB"),
                            XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                            ProcessTime = DateTime.Now.ToString()
                        }).ToList();


            Azuretableoperation azuretableoperation = new Azuretableoperation();
            ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();

            StringBuilder sb = new StringBuilder();
            string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

            //lblReservationStatus.Text = "Total " + lstPmsReservation.Count().ToString() + " Number Received By PMS";
            rtStatus.Text = sb.Append(startTime).ToString();
            rtStatus.Text = sb.Append("   Reservation Process Initialize.... \n").ToString();
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            Thread.Sleep(3000);

            rtStatus.Text = "Reservation Transfer Process started PMS to FXCRS";
            rtStatus.Text = sb.Append(startTime).ToString();
            rtStatus.Text = sb.Append("Reservation Transfer Process started PMS to FXCRS \n").ToString();
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            foreach (var item in lstPmsReservation)
            {
                string xmlmessage = string.Empty;
                xmlmessage = item.XmlMessages;

                //_outResponse = azuretableoperation.PmsHotelPositionDataLog(AzureAccountName, AzureStorageKey,
                //    AzureStorageTable, item).Result;

                var token = Guid.NewGuid().ToString();

                try
                {
                    var _output = SendToGatewayHotel(xmlmessage, item.PropertyCode.ToString(), token);

                    if (_output.MESSAGE == "Success")
                    {

                    }

                    //rtStatus.Text = string.Format( "Reservation {0}",DateTime.Now.ToString("yyyy-MM-dd HHmm ss"));
                    //rtStatus.ForeColor = Color.Green;
                    //rtStatus.BackColor = Color.White;

                    rtStatus.Text = sb.Append(startTime).ToString();
                    rtStatus.Text = sb.Append("Hotel Position Trasfering... \n").ToString();
                    rtStatus.ForeColor = Color.Green;
                    rtStatus.BackColor = Color.White;

                    Thread.Sleep(5000);

                }
                catch (Exception ex)
                {
                    OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                }

            }
            btn_ReservationExport.Enabled = true;
            btn_ReservationExport.ForeColor = Color.Black;

        }

        private void btn_ReservationExport_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            

            btn_ReservationExport.Enabled = false;
            btn_ReservationExport.ForeColor = Color.Green;


            string _Output = string.Empty;
            var _OutputResponse = new OutputResponseModel();
            DataSet DataSet = new DataSet();
            Dictionary<string, object> ResponseData = new Dictionary<string, object>();
            try
            {
                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
                var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageReservationTable"];
                var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureReservationQueue"];
                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];


                string _outResponse = string.Empty;

                int pageSize = 100;
                

                // getting data from database 

                string connetionString = null;
                SqlConnection connection;
                SqlDataAdapter adapter;
                SqlCommand command = new SqlCommand();
                DataSet ds = new DataSet();

                connetionString = _dbConnection;
                StringBuilder sqlQuery = new StringBuilder();
                connection = new SqlConnection(connetionString);
                try
                {
                    sqlQuery = DropprocedureString();

                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("procedure has dropped");
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Dropped" + ex.Message);
                    connection.Close();
                }

                if (SqlType == "New")
                {
                    try
                    {
                        sqlQuery = DropFetchprocedureString();

                        connection.Open();
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        command.CommandText = sqlQuery.ToString();
                        adapter = new SqlDataAdapter(command);
                        adapter.Fill(ds);
                        connection.Close();
                        MessageBox.Show("procedure has dropped");
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("Dropped" + ex.Message);
                        connection.Close();
                    }
                }


                sqlQuery.Clear();

                try
                {
                    sqlQuery = procedureString();
                    connection.Open();
                    command.Connection = connection;
                    command.CommandType = CommandType.Text;
                    command.CommandText = sqlQuery.ToString();
                    adapter = new SqlDataAdapter(command);
                    adapter.Fill(ds);
                    connection.Close();
                    MessageBox.Show("Created the procedure");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Created" + ex.Message);
                    connection.Close();
                }


                sqlQuery.Clear();

                if (SqlType == "New")
                {

                    try
                    {
                        sqlQuery = FetchprocedureString();
                        connection.Open();
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        command.CommandText = sqlQuery.ToString();
                        adapter = new SqlDataAdapter(command);
                        adapter.Fill(ds);
                        connection.Close();
                        MessageBox.Show("Fetch Created the procedure");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Created" + ex.Message);
                        connection.Close();
                    }


                }
                   





                //try
                //{                    
                //    connection.Open();
                //    command.Connection = connection;
                //    command.CommandType = CommandType.StoredProcedure;
                //    command.CommandText = "[PMS].[SP_ReservationBulkPull_XML]";
                //    adapter = new SqlDataAdapter(command);
                //    adapter.Fill(ds);
                //    connection.Close();
                //    MessageBox.Show("Executed the procedure");
                //}
                //catch (Exception ex)
                //{
                //    connection.Close();
                //    MessageBox.Show("Executed" + ex.Message);
                //}



                try
                {
                    if(SqlType == "New")
                    {
                        for (int pageNumber = 1; ; pageNumber++)
                        {
                            connection.Open();
                            ds.Clear();
                            command.Connection = connection;
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@pagenumber", pageNumber);
                            command.Parameters.AddWithValue("@pagesize", pageSize);
                            command.CommandText = "[fetchbulkrReservation]";
                            adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            connection.Close();
                            MessageBox.Show("Executed the procedure to fetch data");
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                exportData(ds);
                                continue;
                            }
                            else
                            {
                                break;
                            }

                        }
                    }
                    if(SqlType == "Old")
                    {
                        connection.Open();
                        ds.Clear();
                        command.Connection = connection;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "SELECT * FROM [PMS].BULKRESERVATION";
                        adapter = new SqlDataAdapter(command);
                        adapter.Fill(ds);
                        connection.Close();
                        MessageBox.Show("Executed the procedure to fetch data");
                        exportData(ds);
                       
                    }

                    MessageBox.Show(" Data Exported Successfully !! ");

                }
                catch(Exception ex)
                {
                    connection.Close();
                    MessageBox.Show("Executed" + ex.Message);
                }
                


                // data has fetched from db

                //List<PMSDataReservationRequest> lstPmsReservation = ds.Tables[0].AsEnumerable().Select(
                //            dataRow => new PMSDataReservationRequest
                //            {
                //                SerialNo = dataRow.Field<int>("ROWNUB"),
                //                PropertyCode = dataRow.Field<long>("PMSCODE"),
                //                ReservationNumber = dataRow.Field<decimal>("RESNUB"),
                //                XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                //                ProcessTime = DateTime.Now.ToString()
                //            }).ToList();

               

                //Azuretableoperation azuretableoperation = new Azuretableoperation();
                //ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();



                ////lblReservationStatus.Text = "Total " + lstPmsReservation.Count().ToString() + " Number Received By PMS";
                //rtStatus.Text = sb.Append(startTime).ToString();
                //rtStatus.Text = sb.Append("   Reservation Process Initialize.... \n").ToString();
                //rtStatus.ForeColor = Color.Green;
                //rtStatus.BackColor = Color.White;

                //Thread.Sleep(3000);

                ////rtStatus.Text = "Reservation Transfer Process started PMS to FXCRS";
                //rtStatus.Text = sb.Append(startTime).ToString();
                //rtStatus.Text = sb.Append("Reservation Transfer Process started PMS to FXCRS \n").ToString();
                //rtStatus.ForeColor = Color.Green;
                //rtStatus.BackColor = Color.White;


                //foreach (var item in lstPmsReservation)
                //{
                //    string xmlmessage = string.Empty;
                //    xmlmessage = item.XmlMessages;

                //    _outResponse = azuretableoperation.PmsReservationDataLog(AzureAccountName, AzureStorageKey,
                //        AzureStorageTable, item).Result;

                //    try
                //    {
                //        var _output = SendToGateway(xmlmessage, item.PropertyCode.ToString(), _outResponse);

                //        if (_output.MESSAGE == "Success")
                //        {

                //        }

                //        //rtStatus.Text = string.Format( "Reservation {0}",DateTime.Now.ToString("yyyy-MM-dd HHmm ss"));
                //        //rtStatus.ForeColor = Color.Green;
                //        //rtStatus.BackColor = Color.White;

                //        rtStatus.Text = sb.Append(startTime).ToString();
                //        rtStatus.Text = sb.Append("Reservation Trasfering... \n").ToString();
                //        rtStatus.ForeColor = Color.Green;
                //        rtStatus.BackColor = Color.White;

                //        Thread.Sleep(5000);

                //    }
                //    catch (Exception ex)
                //    {
                //        OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                //    }

                //}
                //btn_ReservationExport.Enabled = true;
                //btn_ReservationExport.ForeColor = Color.Black;
                //MessageBox.Show(" Data Exported Successfully !! ");
            }
            catch (Exception ex)
            {
                _Output = ex.Message.ToString();
            }
        }

        public void exportCompanyData(DataSet CompanyData)
        {
            string _Output = string.Empty;
            try
            {


                var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
                var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
                var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageCompanyTable"];
                var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureCompanyQueue"];
                var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];

                string _outResponse = string.Empty;

                string connetionString = null;
                SqlConnection connection;
                connetionString = _dbConnection;
                connection = new SqlConnection(connetionString);
                var _OutputResponse = new OutputResponseModel();
               


                List<PMSDataCompanyRequest> lstPmsCompany = CompanyData.Tables[0].AsEnumerable().Select(
                                dataRow => new PMSDataCompanyRequest
                                {
                                    SerialNo = Convert.ToInt64(dataRow.Field<int>("srlnub")),
                                    PropertyCode = dataRow.Field<long>("pmscode"),
                                    ComCode = dataRow.Field<string>("comcod"),
                                    XmlMessages = dataRow.Field<string>("xmlmessage"),
                                    ProcessTime = DateTime.Now.ToString(),
                                    
                                }).ToList();

                connection.Close();

                Azuretableoperation azuretableoperation = new Azuretableoperation();
                ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();

                foreach (var item in lstPmsCompany)
                {

                    string xmlmessage = string.Empty;
                    xmlmessage = item.XmlMessages;

                    item.Token = Guid.NewGuid().ToString();

                    CompanyRequestModel incomming = new CompanyRequestModel();
                    incomming.DhXmlMessage = item.XmlMessages;
                    incomming.PMSCustCode = item.PropertyCode.ToString();
                    incomming.TokenNumber = item.Token;

                    item.XmlMessages = JsonConvert.SerializeObject(incomming);

                    _outResponse = azuretableoperation.PmsCompanyDataLog(AzureAccountName, AzureStorageKey, AzureStorageTable, item).Result;


                    try
                    {
                       

                        var _output = SendToGatewayCompany(incomming);

                    }
                    catch (Exception ex)
                    {
                        OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                    }

                }

                //MessageBox.Show(" Data Exported Successfully !! ");
            }
            catch(Exception ex)
            {
                _Output = ex.Message.ToString();
            }

        }

        private void exportData(DataSet reservations)
        {
            var AzureAccountName = ConfigurationManager.AppSettings["AzureAccountName"];
            var AzureStorageKey = ConfigurationManager.AppSettings["AzureStorageKey"];
            var AzureStorageTable = ConfigurationManager.AppSettings["AzureStorageReservationTable"];
            var AzureCompanyQueue = ConfigurationManager.AppSettings["AzureReservationQueue"];
            var AzureServiceBus = ConfigurationManager.AppSettings["ServiceBus"];


            string _outResponse = string.Empty;

            var _OutputResponse = new OutputResponseModel();

            // data has fetched from db

            List<PMSDataReservationRequest> lstPmsReservation = reservations.Tables[0].AsEnumerable().Select(
                        dataRow => new PMSDataReservationRequest
                        {
                            SerialNo = dataRow.Field<int>("ROWNUB"),
                            PropertyCode = dataRow.Field<long>("PMSCODE"),
                            ReservationNumber = dataRow.Field<decimal>("RESNUB"),
                            XmlMessages = dataRow.Field<string>("XMLMESSAGE"),
                            ProcessTime = DateTime.Now.ToString()
                        }).ToList();


            Azuretableoperation azuretableoperation = new Azuretableoperation();
            ServiceBusQueueMessage serviceBusQueueMessage = new ServiceBusQueueMessage();

            StringBuilder sb = new StringBuilder();
            string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

            //lblReservationStatus.Text = "Total " + lstPmsReservation.Count().ToString() + " Number Received By PMS";
            rtStatus.Text = sb.Append(startTime).ToString();
            rtStatus.Text = sb.Append("   Reservation Process Initialize.... \n").ToString();
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            Thread.Sleep(3000);

            rtStatus.Text = "Reservation Transfer Process started PMS to FXCRS";
            rtStatus.Text = sb.Append(startTime).ToString();
            rtStatus.Text = sb.Append("Reservation Transfer Process started PMS to FXCRS \n").ToString();
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            foreach (var item in lstPmsReservation)
            {
                string xmlmessage = string.Empty;
                xmlmessage = item.XmlMessages;

                _outResponse = azuretableoperation.PmsReservationDataLog(AzureAccountName, AzureStorageKey,
                    AzureStorageTable, item).Result;

                try
                {
                    var _output = SendToGateway(xmlmessage, item.PropertyCode.ToString(), _outResponse);

                    if (_output.MESSAGE == "Success")
                    {

                    }

                    //rtStatus.Text = string.Format( "Reservation {0}",DateTime.Now.ToString("yyyy-MM-dd HHmm ss"));
                    //rtStatus.ForeColor = Color.Green;
                    //rtStatus.BackColor = Color.White;

                    rtStatus.Text = sb.Append(startTime).ToString();
                    rtStatus.Text = sb.Append("Reservation Trasfering... \n").ToString();
                    rtStatus.ForeColor = Color.Green;
                    rtStatus.BackColor = Color.White;

                    Thread.Sleep(5000);

                }
                catch (Exception ex)
                {
                    OutputHandler.OutputErrorResponse(ex, ref _OutputResponse);
                }

            }
            btn_ReservationExport.Enabled = true;
            btn_ReservationExport.ForeColor = Color.Black;

        }

        private StringBuilder DropprocedureString()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine(" IF EXISTS( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PMS].[SP_ReservationBulkPull_XML]') AND type IN ( N'P', N'PC') ) ");
            sb.AppendLine("  begin  ");
            sb.AppendLine(" DROP procedure [PMS].[SP_ReservationBulkPull_XML] ;");
            sb.AppendLine(" END");


            return sb;
        }

        private StringBuilder DropFetchprocedureString()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine(" IF EXISTS( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fetchbulkrReservation]') AND type IN ( N'P', N'PC') ) ");
            sb.AppendLine("  begin  ");
            sb.AppendLine(" DROP procedure [fetchbulkrReservation] ;");
            sb.AppendLine(" END");


            return sb;
        }

        private StringBuilder DropCompanyProcedure()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine(" IF EXISTS( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PMS].[SP_CompnayBulkPull_XML]') AND type IN ( N'P', N'PC') ) ");
            sb.AppendLine("  begin  ");
            sb.AppendLine(" DROP procedure [PMS].[SP_CompnayBulkPull_XML] ;");
            sb.AppendLine(" END");


            return sb;
        }

        private StringBuilder DropFetchcompanyProcedure()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine(" IF EXISTS( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fetchbulkCompanyProfile]') AND type IN ( N'P', N'PC') ) ");
            sb.AppendLine("  begin  ");
            sb.AppendLine(" DROP procedure [fetchbulkCompanyProfile] ;");
            sb.AppendLine(" END");


            return sb;
        }


        private StringBuilder FetchprocedureString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("CREATE PROCEDURE [dbo].[fetchbulkrReservation] @pagenumber int,@pagesize int AS BEGIN select * from BULKRESERVATION order by rownub Offset  @pagesize * (@pagenumber - 1) rows fetch next @pagesize rows only END");

            return sb;
        }


        private StringBuilder FetchCompanyprocedureString()
        {
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("CREATE PROCEDURE [dbo].[fetchbulkCompanyProfile] \n");
            varname1.Append("  @pagenumber INT, \n");
            varname1.Append("  @pagesize   INT \n");
            varname1.Append("AS \n");
            varname1.Append("  BEGIN \n");
            varname1.Append("    SELECT   * \n");
            varname1.Append("    FROM     bulkcompanyprofile \n");
            varname1.Append("    ORDER BY srlnub offset @pagesize * (@pagenumber - 1) rows \n");
            varname1.Append("    FETCH next @pagesize rows only \n");
            varname1.Append("  END");

            return varname1;
        }


        private StringBuilder procedureString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  create PROCEDURE [PMS].[SP_ReservationBulkPull_XML] AS BEGIN SET NOCOUNT ON DECLARE @PRPCOD VARCHAR(3) DECLARE @PMSCUSTCODE BIGINT DECLARE @ECOTOKEN NVARCHAR(600) DECLARE @DATETIME NVARCHAR(100)=(SELECT CONVERT(VARCHAR(23), GETDATE(), 126)) SELECT TOP(1) @PRPCOD=PRPCOD FROM pms.PR002TBL WHERE DELFLG=0 select @PMSCUSTCODE=CSTCOD from pms.GNMANTBL DECLARE @BULKRESERVATION AS TABLE(ROWNUB INT,PMSCODE BIGINT,RESNUB DECIMAL,SRLNUB DECIMAL,SUBSRL DECIMAL,XMLMESSAGE XML,CREATEDTIME DATETIME) DECLARE @RESERVATIONTABLE AS TABLE(ROWNUB INT, RESNUB DECIMAL,SRLNUB DECIMAL,SUBSRL DECIMAL) DECLARE @RESERVATIONTBL AS TABLE(RESNUB DECIMAL,ROWNUB INT) INSERT INTO @RESERVATIONTABLE SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY RESNUB ASC),RESNUB,SRLNUB,SUBSRL FROM FMR01TBL A WHERE A.SUBSRL = (SELECT MAX(B.SUBSRL) FROM FMR01TBL B WHERE A.PRPCOD=B.PRPCOD AND A.RESNUB = B.RESNUB AND A.SRLNUB = B.SRLNUB) AND (A.SGLBKD +A.DBLBKD+ A.TPLBKD+A.QUDBKD) <> 0 AND A.ARRDAT >= (SELECT ACCDAT FROM pms.GNACDTBL) AND A.CKIROM = 0 AND A.CKIPAX = 0 INSERT INTO @RESERVATIONTBL SELECT RESNUB, ROW_NUMBER() OVER(ORDER BY RESNUB ASC) AS ROWNUB FROM(SELECT DISTINCT RESNUB FROM @RESERVATIONTABLE) A DECLARE @TOTALROW INT=0 DECLARE @CURRENTROW INT=1 SELECT @TOTALROW=COUNT(1) FROM @RESERVATIONTBL select * into #tmpFmr01tbl from pms.fmr01tbl where 1=2 select * into #tmpFmr14tbl from pms.fmr14tbl where 1=2 WHILE (@CURRENTROW<=@TOTALROW) BEGIN DECLARE @CUR_RESNUB decimal=0 DECLARE @CUR_SRLNUB decimal=0 DECLARE @CUR_SUBSRL decimal=0 delete from #tmpFmr01tbl delete from #tmpFmr14tbl DECLARE @MAINXML XML='' DECLARE @PROPERTY XML ='' DECLARE @FMR00TBL XML='' DECLARE @FMR01TBL XML='' DECLARE @FMR02TBL XML='' DECLARE @FMR03TBL XML='' DECLARE @FMR04TBL XML='' DECLARE @FMR05TBL XML='' DECLARE @FMR06TBL XML='' DECLARE @FMR07TBL XML='' DECLARE @FMR08TBL XML='' DECLARE @FMR09TBL XML='' DECLARE @FMR10TBL XML='' DECLARE @FMR11TBL XML='' DECLARE @FMR12TBL XML='' DECLARE @FMR13TBL XML='' DECLARE @FMR14TBL XML='' DECLARE @FMR15TBL XML='' DECLARE @FMR16TBL XML='' DECLARE @FMR17TBL XML='' DECLARE @FMR18TBL XML='' DECLARE @FMR19TBL XML='' DECLARE @FMR25TBL XML='' DECLARE @FMR26TBL XML='' DECLARE @FMRDCTBL XML='' DECLARE @FMRSNTBL XML='' DECLARE @FMRCHTBL XML='' DECLARE @CM_BOOKING_COMMENTS XML='' SELECT @ECOTOKEN=NEWID() SET @MAINXML='<SKYRESDATA><PROPERTYDETAILS></PROPERTYDETAILS></SKYRESDATA>' SET @PROPERTY='<APPLICATIONNAME>SKYRES</APPLICATIONNAME> <PRPCOD>'+@PRPCOD+'</PRPCOD> <PMSCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</PMSCODE> <PLATFORMCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</PLATFORMCODE> <CHAINCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</CHAINCODE> <TABLENAME>RESERVATION</TABLENAME> <OPERATIONMODE>INSERT</OPERATIONMODE> <ECOTOKENNO>'+@ECOTOKEN+'</ECOTOKENNO> <TIMESTAMP>'+@DATETIME+'</TIMESTAMP>' SELECT @CUR_RESNUB= RESNUB FROM @RESERVATIONTBL WHERE ROWNUB=@CURRENTROW insert into #tmpFmr01tbl SELECT * FROM FMR01TBL A WHERE A.SUBSRL = (SELECT MAX(B.SUBSRL) FROM FMR01TBL B WHERE A.PRPCOD=B.PRPCOD AND A.RESNUB = B.RESNUB AND A.SRLNUB = B.SRLNUB) AND RESNUB=@CUR_RESNUB update #tmpFmr01tbl set subsrl=1 insert into #tmpFmr14tbl SELECT * FROM FMR14TBL A WHERE A.SUBSRL = (SELECT MAX(B.SUBSRL) FROM FMR01TBL B WHERE A.RESNUB = B.RESNUB AND A.SRLNUB = B.SRLNUB) AND RESNUB=@CUR_RESNUB update #tmpFmr14tbl set subsrl=1 set @FMR00TBL=(SELECT * FROM FMR00TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR01TBL=(SELECT * FROM #tmpFmr01tbl as FMR01TBL FOR XML AUTO,ELEMENTS) SET @FMR02TBL=(SELECT * FROM FMR02TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR03TBL=(SELECT * FROM FMR03TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR04TBL=(SELECT * FROM FMR04TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR05TBL=(SELECT * FROM FMR05TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR06TBL=(SELECT * FROM FMR06TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR07TBL=(SELECT * FROM FMR07TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR08TBL=(SELECT * FROM FMR08TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR09TBL=(SELECT * FROM FMR09TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR10TBL=(SELECT * FROM FMR10TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR11TBL=(SELECT * FROM FMR11TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR12TBL=(SELECT * FROM FMR12TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR13TBL=(SELECT * FROM FMR13TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR14TBL=(SELECT * FROM #tmpFmr14tbl FMR14TBL FOR XML AUTO,ELEMENTS) SET @FMR15TBL=(SELECT * FROM FMR15TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR16TBL=(SELECT * FROM FMR16TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR17TBL=(SELECT * FROM FMR17TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR18TBL=(SELECT * FROM FMR18TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR19TBL=(SELECT * FROM FMR19TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR25TBL=(SELECT * FROM FMR25TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMR26TBL=(SELECT * FROM FMR26TBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMRDCTBL=(SELECT * FROM FMRDCTBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMRSNTBL=(SELECT * FROM FMRSNTBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @FMRCHTBL=(SELECT * FROM FMRCHTBL WHERE RESNUB=@CUR_RESNUB FOR XML AUTO,ELEMENTS) SET @MAINXML.modify('insert sql:variable(\"@PROPERTY\") as first into (/SKYRESDATA/PROPERTYDETAILS)[1]') SET @MAINXML.modify('insert sql:variable(\"@FMR00TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR01TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR02TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR03TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR04TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR05TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR06TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR07TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR08TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR09TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR10TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR11TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR12TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR13TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR14TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR15TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR16TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR17TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR18TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR19TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR25TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMR26TBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMRDCTBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMRSNTBL\") as last into (/SKYRESDATA[1])') SET @MAINXML.modify('insert sql:variable(\"@FMRCHTBL\") as last into (/SKYRESDATA[1])') INSERT INTO [PMS].BULKRESERVATION SELECT @CURRENTROW,@PMSCUSTCODE,@CUR_RESNUB,@CUR_SRLNUB,@CUR_SUBSRL,@MAINXML,GETDATE() SET @CURRENTROW+=1 END SELECT * FROM [PMS].BULKRESERVATION  ORDER BY 1 ASC END ");

            return sb;
        }

        private StringBuilder companyProfileprocedureString()
        {
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("CREATE PROCEDURE [PMS].[SP_CompnayBulkPull_XML] \n");
            varname1.Append("  \n");
            varname1.Append("AS \n");
            varname1.Append("BEGIN \n");
            varname1.Append("    SET NOCOUNT ON \n");
            varname1.Append("	--Property Full Name   \n");
            varname1.Append("	DECLARE @PRPCOD VARCHAR(3) \n");
            varname1.Append("	declare @pmscustCode bigint \n");
            varname1.Append("	--ECOTOKEN Number   \n");
            varname1.Append("	DECLARE @ECOTOKEN NVARCHAR(600) \n");
            varname1.Append("	SELECT @ECOTOKEN=NEWID() \n");
            varname1.Append("  \n");
            varname1.Append("		DECLARE @DATETIME NVARCHAR(100)=(SELECT CONVERT(VARCHAR(23), GETDATE(), 126)) \n");
            varname1.Append("  \n");
            varname1.Append("		SELECT  TOP (1) @PRPCOD=PRPCOD FROM PR002TBL where DELFLG=0 \n");
            varname1.Append("  \n");
            varname1.Append("		DECLARE @BULKCOMPANY AS TABLE(SRLNUB INT,PMSCODE BIGINT,COMCOD VARCHAR(50),XMLMESSAGE XML,CREATEDTIME DATETIME ) \n");
            varname1.Append("		DECLARE @COMPANYCODETABLE AS TABLE(ROWNUB int, COMCOD VARCHAR(50)) \n");
            varname1.Append("  \n");
            varname1.Append("		insert into @COMPANYCODETABLE \n");
            varname1.Append("	    select distinct ROW_NUMBER() OVER(ORDER BY COMCOD ASC) , COMCOD from FMCOMTBL A \n");
            varname1.Append("		where A.COMCOD in (select distinct COMCOD from FMCOMTBL b where a.COMCOD=b.COMCOD ) \n");
            varname1.Append("  \n");
            varname1.Append("  \n");
            varname1.Append("		  \n");
            varname1.Append("		DECLARE @TotalRow INT=0 \n");
            varname1.Append("		DECLARE @CurrentRow INT=1 \n");
            varname1.Append("		SELECT @TotalRow=COUNT(1) FROM @COMPANYCODETABLE \n");
            varname1.Append("  \n");
            varname1.Append("		WHILE (@CurrentRow<=@TotalRow) \n");
            varname1.Append("		BEGIN \n");
            varname1.Append("			declare @currentComcod varchar(50)='' \n");
            varname1.Append("			--XML - Property Root  \n");
            varname1.Append("			DECLARE @MAINXML XML='' \n");
            varname1.Append("			DECLARE @PROPERTY XML ='' \n");
            varname1.Append("			DECLARE @FMCOMTBL XML ='' \n");
            varname1.Append("			DECLARE @FMCPDTBL XML ='' \n");
            varname1.Append("			DECLARE @FMCWLTBL XML ='' \n");
            varname1.Append("			DECLARE @FMCBLTBL XML ='' \n");
            varname1.Append("			DECLARE @SL030TBL XML ='' \n");
            varname1.Append("			DECLARE @SLCAMTBL XML ='' \n");
            varname1.Append("  \n");
            varname1.Append("			--XML - ROOT   \n");
            varname1.Append("	  \n");
            varname1.Append("		SET @MAINXML='<SkyResData><PropertyDetails></PropertyDetails></SkyResData>' \n");
            varname1.Append("  \n");
            varname1.Append("			--XML TOP Property Section Node  \n");
            varname1.Append("		SET @PROPERTY='<ApplicationName>SkyRes</ApplicationName>  \n");
            varname1.Append("		<PRPCOD>'+@PRPCOD+'</PRPCOD>  \n");
            varname1.Append("		<PMSCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</PMSCODE>  \n");
            varname1.Append("		<PLATFORMCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</PLATFORMCODE>  \n");
            varname1.Append("		<CHAINCODE>'+CAST(@PRPCOD AS NVARCHAR(MAX))+'</CHAINCODE>  \n");
            varname1.Append("		<TABLENAME>Reservation</TABLENAME>  \n");
            varname1.Append("		<OPERATIONMODE>INSERT</OPERATIONMODE>  \n");
            varname1.Append("		<ECOTOKENNO>'+@ECOTOKEN+'</ECOTOKENNO>  \n");
            varname1.Append("		<TIMESTAMP>'+@DATETIME+'</TIMESTAMP>' \n");
            varname1.Append("  \n");
            varname1.Append("			select @currentComcod=comcod from @COMPANYCODETABLE where ROWNUB=@CurrentRow \n");
            varname1.Append("  \n");
            varname1.Append("			SET @FMCOMTBL=(SELECT * FROM FMCOMTBL WHERE COMCOD=@CURRENTCOMCOD  FOR XML AUTO,ELEMENTS) \n");
            varname1.Append("			SET @FMCPDTBL=(SELECT * FROM FMCPDTBL WHERE COMCOD=@CURRENTCOMCOD  FOR XML AUTO,ELEMENTS) \n");
            varname1.Append("			SET @FMCWLTBL=(SELECT * FROM FMCWLTBL WHERE COMCOD=@CURRENTCOMCOD  FOR XML AUTO,ELEMENTS) \n");
            varname1.Append("			SET @FMCBLTBL=(SELECT * FROM FMCBLTBL WHERE COMCOD=@CURRENTCOMCOD  FOR XML AUTO,ELEMENTS) \n");
            varname1.Append("			--SET @FMCOMTBL=(SELECT * FROM SL030TBL WHERE COMCOD='BNKCANB' FOR XML AUTO,ELEMENTS)  \n");
            varname1.Append("			SET @SLCAMTBL=(SELECT * FROM SLCAMTBL WHERE COMCOD=@CURRENTCOMCOD FOR XML AUTO,ELEMENTS) \n");
            varname1.Append("  \n");
            varname1.Append("			--First Row   \n");
            varname1.Append("				SET @MAINXML.modify('insert sql:variable(\"@PROPERTY\") as first into (/SkyResData/PropertyDetails)[1]') \n");
            varname1.Append("				SET @MAINXML.modify('insert sql:variable(\"@FMCOMTBL\") as last into (/SkyResData)[1]') \n");
            varname1.Append("				SET @MAINXML.modify('insert sql:variable(\"@FMCPDTBL\") as last into (/SkyResData)[1]') \n");
            varname1.Append("				SET @MAINXML.modify('insert sql:variable(\"@FMCWLTBL\") as last into (/SkyResData)[1]') \n");
            varname1.Append("				SET @MAINXML.modify('insert sql:variable(\"@FMCBLTBL\") as last into (/SkyResData)[1]') \n");
            varname1.Append("				SET @MAINXML.modify('insert sql:variable(\"@SL030TBL\") as last into (/SkyResData)[1]') \n");
            varname1.Append("				SET @MAINXML.modify('insert sql:variable(\"@SLCAMTBL\") as last into (/SkyResData)[1]') \n");
            varname1.Append("  \n");
            varname1.Append("			--insert into @bulkCompany  \n");
            varname1.Append("			--select @CurrentRow,@pmscustCode,@currentComcod,@MAINXML,getdate()			  \n");
            varname1.Append("  \n");
            varname1.Append("			 INSERT INTO BULKCOMPANYPROFILE \n");
            varname1.Append("			 select @CurrentRow,@pmscustCode,@currentComcod,@MAINXML,getdate() \n");
            varname1.Append("  \n");
            varname1.Append("  \n");
            varname1.Append("			SET @CurrentRow+=1 \n");
            varname1.Append("	end \n");
            varname1.Append("	    --select * from @bulkCompany order by 1 asc  \n");
            varname1.Append("		  SELECT * \n");
            varname1.Append("		  FROM   BULKCOMPANYPROFILE \n");
            varname1.Append("		  ORDER  BY 1 ASC \n");
            varname1.Append("END");

            return varname1;
        }

        //procedure name need to be change
        private StringBuilder DropHotelPositionProcedure()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine(" IF EXISTS( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[PMS].[SP_CompnayBulkPull_XML]') AND type IN ( N'P', N'PC') ) ");
            sb.AppendLine("  begin  ");
            sb.AppendLine(" DROP procedure [PMS].[SP_CompnayBulkPull_XML] ;");
            sb.AppendLine(" END");


            return sb;
        }

        //procedure name need to be change
        private StringBuilder DropFetchHotelPositionProcedure()
        {
            StringBuilder sb = new StringBuilder();


            sb.AppendLine(" IF EXISTS( SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[fetchbulkCompanyProfile]') AND type IN ( N'P', N'PC') ) ");
            sb.AppendLine("  begin  ");
            sb.AppendLine(" DROP procedure [fetchbulkCompanyProfile] ;");
            sb.AppendLine(" END");


            return sb;
        }

        //need to add the procedure.
        private StringBuilder HotelPositionprocedureString()
        {
            StringBuilder varname1 = new StringBuilder();

            return varname1;
        }

        //need to add the procedure
        private StringBuilder HotelPositionFetchprocedureString()
        {
            StringBuilder sb = new StringBuilder();

            

            return sb;
        }

        private DHResponseModel SendToGateway(string IncommingXMlMessage, string pmscode,string tokenNumber)
        {

            string url = string.Empty;
            string content = string.Empty;

            // content = new WebClient().DownloadString(IncommingXMlMessage);

            rtStatus.Text = "Reservation Transfer Process Processing....";
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            DHResponseModel dHResponseModel = new DHResponseModel();
            //IncommingModel incomming = new IncommingModel();
            //incomming.PMSCUSTCODE = Convert.ToInt64(pmscode);
            //incomming.PMSCUSTID = 0;
            //incomming.TOKENNUMBER = Guid.NewGuid().ToString();
            //incomming.INCOMINGMESSAGE = IncommingXMlMessage; //JsonConvert.SerializeObject(IncommingXMlMessage);
            //incomming.RECEIVEDDATE = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            //incomming.DATASOURCE = "DATAHUB";
            //incomming.DATADESTINATION = "FXFD";
            //incomming.MESSAGETYPE = "Reservation";

            IncomingMessageModel incomming = new IncomingMessageModel();
            incomming.DhXmlMessage = IncommingXMlMessage;
            incomming.PMSCustCode = Convert.ToInt64(pmscode);
            incomming.TokenNumber = tokenNumber;

            if (content != null)
            {

                //incomming = JsonConvert.DeserializeObject<IncommingModel>(content);

                //string _reservationUrl = "https://fxdistributionreservationgateway.azurewebsites.net/api/InReservation/ReservationData";
                //string _reservationUrl = "https://qafxdistributionreservationgateway.azurewebsites.net/api/InReservation/ReservationData";
                //string _reservationUrl = "https://qacrsbulkreservation.azurewebsites.net/api/BulkReservation/BulkReservation";
                string _reservationUrl = "https://crsbulkreservation.azurewebsites.net/api/BulkReservation/BulkReservation";

                var _objTojson = JsonConvert.SerializeObject(incomming);
                dHResponseModel = SharkPost(_reservationUrl, incomming);

            }
            return dHResponseModel;

        }

        private DHResponseModel SendToGatewayCompany(CompanyRequestModel model)
        {
            
            DHResponseModel dHResponseModel = new DHResponseModel();

            //string _companyProfileUrl = "https://crsbulkcompany.azurewebsites.net/api/BulkCompany/DataProcess";
            string _companyProfileUrl = "https://crsbulkcompanyprod.azurewebsites.net/api/BulkCompany/DataProcess";

            var _objTojson = JsonConvert.SerializeObject(model);
            
            dHResponseModel = SharkPost(_companyProfileUrl, model);

            return dHResponseModel;
        }

        //need to change
        private DHResponseModel SendToGatewayHotel(string IncommingXMlMessage, string pmscode, string tokenNumber)
        {

            string url = string.Empty;
            string content = string.Empty;

           

            rtStatus.Text = "Reservation Transfer Process Processing....";
            rtStatus.ForeColor = Color.Green;
            rtStatus.BackColor = Color.White;

            DHResponseModel dHResponseModel = new DHResponseModel();
            

            IncomingMessageModel incomming = new IncomingMessageModel();
            incomming.DhXmlMessage = IncommingXMlMessage;
            incomming.PMSCustCode = Convert.ToInt64(pmscode);
            incomming.TokenNumber = tokenNumber;

            if (content != null)
            {
                string _hotelPositionUrl = ConfigurationManager.AppSettings["HotelPostionAPIUrl"];
                //string _reservationUrl = "https://crsbulkreservation.azurewebsites.net/api/BulkReservation/BulkReservation";

                var _objTojson = JsonConvert.SerializeObject(incomming);
                dHResponseModel = SharkPost(_hotelPositionUrl, incomming);

            }
            return dHResponseModel;

        }


        public DHResponseModel SharkPost(string Url, object model)
        {
            var _Output = new DHResponseModel();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Url);
                    var Records = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                    var response = client.PostAsync(Url, Records).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        _Output.STATUS = true;
                        _Output.MESSAGE = response.StatusCode.ToString();
                    }
                    else
                    {
                        var responseContent = response.Content;

                        var _pb = responseContent.ReadAsStringAsync();
                        string _OutputConvertToString = _pb.Result.ToString();
                        _Output = JsonConvert.DeserializeObject<DHResponseModel>(_OutputConvertToString);
                    }
                }


            }
            catch (Exception ex)
            {
                _Output.STATUS = false;
                _Output.MESSAGE = ex.Message.ToString();
            }
            return _Output;

        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            //rtLog.Text = "";
        }

        private void comboBoxSQLType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxSQLType.SelectedIndex == 1)
            {
               SqlType = "New";
                MessageBox.Show(SqlType);
            }
            else if (comboBoxSQLType.SelectedIndex == 2)
            {
                SqlType = "Old";
                MessageBox.Show(SqlType);
            }
        }
    }

    public class PMSDataCompanyRequest
    {
        public string PartitionKey { get; set; } 
        public string RowKey { get; set; } 
        public long SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public string ComCode { get; set; }
        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }

        public string Token { get; set; }
    }

    public class PMSHotelPositionRequest
    {
       
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public long SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public string ComCode { get; set; }
        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }

        public string Rundate { get; set; }

        public string Token { get; set; }
    }

    public class PMSDataReservationRequest
    {
        public string PartitionKey { get; set; } = "9800";
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public int SerialNo { get; set; }
        public long PropertyCode { get; set; }
        public decimal ReservationNumber { get; set; }

        public string XmlMessages { get; set; }
        public string ProcessTime { get; set; }
        public string MessageStatus { get; set; }
    }
    public class ServiceBusQueueMessage
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; } = Guid.NewGuid().ToString();
        public string XMLMessageUrl { get; set; }

    }
    public class IncomingMessageModel
    {
        public string DhXmlMessage { get; set; }
        public long PMSCustCode { get; set; }
        public string TokenNumber { get; set; }
    }

    public class CompanyRequestModel
    {
        public string DhXmlMessage { get; set; }
        public string PMSCustCode { get; set; }
        public string TokenNumber { get; set; }
    }
}
