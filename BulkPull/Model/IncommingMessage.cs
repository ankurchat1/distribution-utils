﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkPull.Model
{
    public class IncommingModel
    {
        /// <summary>
        /// Customer id ,which is unique
        /// </summary>
        public long PMSCUSTCODE { get; set; }
        /// <summary>
        /// Fxcrs id , whcih is unique created for the property in Fxcrs
        /// </summary>
        public long PMSCUSTID { get; set; }
        /// <summary>
        /// From where the message has generated
        /// </summary>
        public string DATASOURCE { get; set; }
        /// <summary>
        /// Where to send the message FXFD/Datahub or other interface
        /// </summary>
        public string DATADESTINATION { get; set; }
        /// <summary>
        /// Created Date
        /// </summary>
        public string RECEIVEDDATE { get; set; }
        /// <summary>
        /// What kind of incomming message ex:Reservation/Rate etc
        /// </summary>
        public string MESSAGETYPE { get; set; }
        /// <summary>
        /// Unique Identitification 
        /// </summary>
        public string TOKENNUMBER { get; set; }
        /// <summary>
        /// Bundle of Data XML OR JSON OR any string 
        /// </summary>
        public string INCOMINGMESSAGE { get; set; }
        /// <summary>
        /// Create date once the
        /// </summary>
        public string CREATEDDATE { get; set; }

        public string CHANNELTYPE { get; set; }

        public IncommingModel()
        {
            CREATEDDATE = DateTime.Now.AddHours(5).AddDays(30).ToString("yyyy-MM-ddTHH:mm:ss.fff");
        }

    }

    public class DHResponseModel
    {
        public bool STATUS { get; set; }
        public string MESSAGE { get; set; }
    }
}
