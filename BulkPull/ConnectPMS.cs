﻿using IDSTDECEncryptDecrypt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BulkPull
{
    public partial class ConnectPMS : Form
    {
        public ConnectPMS()
        {
            InitializeComponent();
        }


        string _path = string.Empty;
        public string FullPathDB { get; set; }

        public string getData()
        {
            return FullPathDB;
        }

        private void btnClick1_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    lblFolderPath.Text = dialog.SelectedPath;
                    _path = dialog.SelectedPath;

                    var di = new DirectoryInfo(_path);
                    var allFiles = di.GetFiles("*", SearchOption.AllDirectories);
                    foreach (var file in allFiles)
                    {
                        filepath filepath = new filepath();
                        if (file.Extension == ".INI")
                        {
                            filepath._filePathINI = file.FullName.ToString();
                        }
                        if (file.Extension == ".SYS")
                        {
                            filepath._filePathSYS = file.FullName.ToString();
                        }
                        if (file.Extension == ".MDB")
                        {
                            filepath._filePathMDB = file.FullName.ToString();
                        }

                    }


                }


                
            }            
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            MDBDataRead read = new MDBDataRead();
            string DBConnString = read.ReadDBString(filepath._filePathINI, filepath._filePathSYS, filepath._filePathMDB);                       
            FullPathDB = DBConnString;
            Form1 frm = new Form1(FullPathDB);
            frm.Show();
        }

        private void btncancel_Click(object sender, EventArgs e)
        {
            SQLConnectionBuilder obj1 = new SQLConnectionBuilder();
            obj1.Show();
            this.Hide();
        }
    }


    public class filepath
    {
        public static string _filePathINI { get; set; }
        public static string _filePathSYS { get; set; }
        public static string _filePathMDB { get; set; }        
    }
    
}
